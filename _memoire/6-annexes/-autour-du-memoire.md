La réalisation de ce mémoire a été l'occasion de proposer des articles pour des revues académiques et des communications pour des colloques universitaires, ainsi que de rédiger des articles pour des sites ou des carnets, voici une bibliographie qui réunit les initiatives satellites autour de ce travail de recherche :

{% bibliography -f bibliographie-autour %}
