Les ouvrages abordant l'édition et le numérique sont nombreux, bien souvent concentrés sur des questions économiques et focalisés sur la *grande* édition.
La littérature sur ce sujet oublie parfois que les pratiques les plus enrichissantes proviennent d'initiatives alternatives voir expérimentales.
Dans ce paysage éditorial *Post-Digital Print : la mutation de l'édition depuis 1894* {% cite ludovico_post-digital_2016 %} est un sursaut bienvenu doublé d'un travail de recherche remarquable : cet essai est constitué d'une présentation historique et d'une analyse fine de l'évolution de l'*imprimé* au contact du numérique.
Cette traduction française du livre d'Alessandro Ludovico publiée par les éditions B42 est d'un apport majeur.

Alessandro Ludovico est un chercheur, un enseignant, un artiste et un éditeur dont les intérêts et les écrits sont tournés vers le numérique et l'édition alternative depuis plus de vingt ans.
Il est le créateur et rédacteur en chef de la revue *Neural* – fondée en 1993 et consacrée aux arts numériques.

*Post-Digital Print : la mutation de l'édition depuis 1894* est publié en anglais en 2012, suite à un travail de recherche mené à la Hogeschool de Rotterdam, et disponible sous différents formats : un fichier PDF pour une diffusion numérique, une version imprimée à la fois en offset et en impression à la demande pour une distribution physique.
Cette multi-disponibilité reflète ce qu'Alessandro Ludovico expose dans son livre : le papier et le numérique sont désormais complémentaires après plus d'un siècle de cohabitation, de rencontre et parfois de fusion.
*Post-Digital Print* est réédité en 2014 par Onomatopee, puis publié par les éditions B42 en 2016 avec une traduction française de Marie-Mathilde Bortolotti.

Nous nous concentrons ici sur l'ensemble de l'ouvrage en tant que panorama et perspective de la publication en lien avec le numérique.
Nous souhaitons également analyser l'évolution des chaînes de publication comme un des éléments de cette transformation de l'édition esquissée par Alessandro Ludovico.

## 1.1.1. Une rétrospective et une prospective de l'édition
*Post-Digital Print* est divisé en six parties, formant à la fois un parcours historique de l'édition et une analyse précise de projets éditoriaux et alternatifs depuis 1894.
Pourquoi 1894 ?
Il s'agit de l'année de publication d'un ouvrage qui imagine l'édition comme intégrant la voix sous forme directe (via le téléphone) et enregistrée (sur cylindre) : *La Fin des livres* d'Octave Uzanne et Albert Robida.
Le premier chapitre de *Post-Digital Print* est consacré à l'analyse de la supposée "mort du papier" (p. 15) à partir de sept cas illustrant les propos d'Alessandro Ludovico.
Le second chapitre examine l'édition alternative au vingtième siècle, principalement sur la période 1950-1980 qui a vu naître nombre d'initiatives éditoriales liées aux évolutions technologiques, en parallèle d'une édition classique.
Le chapitre trois étudie la transformation du papier avec les bouleversements induits par le *numérique*, l'impression à la demande en est un exemple emblématique.
Le quatrième chapitre explore les expérimentations, projets et entreprises d'édition numérique : que ce soit des plates-formes de vente en ligne, la distribution, les bibliothèques, mais également les artistes.
Le chapitre cinq aborde la question de l'archivage, essentielle dans la problématique du papier en lien avec le numérique : de l'interrogation du support à celle de la constitution d'archives, en passant par la diffusion et la visibilité de celles-ci, passées ou futures.
Le sixième et dernier chapitre s'attarde sur la question du réseau : en quoi transforme-t-il l'édition ?
Et n'est-il pas le moyen pour celle-ci de dépasser la combinaison papier-numérique et de tendre vers une hybridation ?

Prenons le temps d'entrer dans ces différents chapitres pour comprendre l'intérêt de cette approche historique et l'analyse de ces nombreux cas d'édition alternative.

<div class="break"></div>
## 1.1.2. Les six chapitres de Post-Digital Print
Le premier chapitre est consacré à une histoire des media, depuis le télégraphe jusqu'à Internet, à travers laquelle Alessandro Ludovico analyse leurs relations et impacts avec les formats du livre et de la presse.
Différentes inventions – conceptualisées, expérimentées ou adoptées – ont permis au texte imprimé d'évoluer, sans pour autant remettre en cause son existence, même si les prédicateurs de la fin du livre furent nombreux.
L'hypertexte, Internet et la mise en réseau des acteurs du texte sont peut-être les seuls bouleversements en mesure de nous faire envisager une modification profonde de l'imprimé, malgré des limites d'usage.

Dans le second chapitre Alessandro Ludovico explore les évolutions des techniques d'impression en présentant différentes initiatives de publication de mouvements avant-gardistes – activistes, politiques ou artistiques.
Du miméographe à la photocopie en passant par l'offset, des courants comme le futurisme italien, le dadaïsme ou le fanzine punk ont su s'approprier les technologies de reproduction, et qui plus est expérimenter des formes graphiques inédites.
L'édition alternative a été un véritable laboratoire de l'impression, jusqu'au *multimédia* – représenté par le CD-ROM – et au numérique – dont Internet est la figure emblématique.

Les journaux et les magazines ont été les "objets imprimés" (p. 78) qui ont le plus vivement ressentis les mutations de l'édition avec le numérique : c'est l'objet du chapitre 3.
Avec l'influence du web et de ses usages, l'"unité atomique" (p. 67) est passée du journal à l'article, remettant en cause le rôle de l'éditeur via des possibilités de personnalisation des contenus.
L'impression à la demande, sorte de photocopieuse du début du vingt-et-unième siècle, est la matérialisation des processus en œuvre dans l'édition en ligne, notamment avec la facilité et la rapidité de production et de diffusion.
Si d'un côté les magazines recherchent de nouveaux moyens pour résister – via des démarches développant leur visibilité ou par la réintroduction du "geste éditorial" (p. 75) –, l'"automatisation totale" (p. 91) de la publication est désormais accessible, et est le signe sinon la preuve d'un "mariage" entre l'impression et le numérique.

Le quatrième chapitre débute sur un constat : le livre numérique est né d'une intention de partage, intention qui a pu se transformer en une nouvelle forme de lecture grâce à un dispositif de lecture numérique intuitif et convivial, la liseuse à encre électronique.
Cette nouvelle perspective, permise aussi par des sociétés qui usent de renforts marketing très puissants et dont les intentions sont clairement mercantiles, implique des enjeux de design : forme du texte, interface de lecture, reproduction de l'espace du livre, etc.
Le "reformatage" (p. 110) du texte, comme l'appelle Alessandro Ludovico, est une condition du développement et du déploiement de ces nouveaux usages, offerte par le format EPUB.
Comme nous pouvions le prévoir le développement du livre numérique va engendrer une modification de la figure du livre imprimé, ce dernier devenant un objet "rare" et "précieux" (p. 121) via l'utilisation d'éléments difficilement reproduisibles mécaniquement.
Le numérique est alors un outil de diffusion des productions imprimées.
Alessandro Ludovico rappelle, à juste titre, que la dématérialisation a un coût écologique – malgré le discours visant au *sans papier* –, et que les interfaces numériques ne proposent pas encore un confort de lecture similaire aux objets imprimés.
La dualité entre imprimé et numérique est largement exposée dans *Post-Digital Print*, l'objet du chapitre suivant est justement de dépasser cette dualité.

De la même façon que le livre numérique a été une opportunité commerciale, la numérisation rétrospective de l'imprimé portée par des sociétés privées représente une manne financière immense, il ne s'agit donc pas que d'une entreprise patrimoniale : voici comment s'ouvre le cinquième chapitre.
Entre le placement de publicités sur une masse de contenus considérable, et le phénomène d'attraction pour les ouvrages sous droits, peu d'initiatives permettent de concurrencer Google Books, et de proposer des archives réellement ouvertes aux cultures et disponibles librement pour tous.
En dehors de ces projets d'archivage certaines initiatives de numérisation consistent à créer des versions plus exploitables que le papier, avec les techniques de reconnaissance de caractères ou le format PDF.
Pour Alessandro Ludovico le défaut des "petits éditeurs" est d'avoir considéré le web comme un vecteur commercial, et non comme l'occasion de repenser une diffusion culturelle ou une nouvelle démarche de visibilité de leurs activités, deux options bien plus riches justement permises par un dépassement de la *simple* numérisation.
Ce cinquième chapitre se clôture sur le concept de *scrapbooking* qui, à l'ère numérique, est la possibilité de constituer des archives éditorialisées et pérennes.
Un élément clé permet d'envisager cela : le réseau.

Les premières lignes du sixième et dernier chapitre sont un rappel : en dehors de la littérature les livres sont des objets conceptuellement connectés entre eux, par exemple via des citations ou des bibliographies.
Ainsi l'hypertexte ne fait que faciliter la profusion des liens et augmenter la dimension d'un réseau déjà constitué.
Le numérique renforce cette nécessité d'une mise en réseau entre éditeurs, nœuds d'une structure distribuée.
La distribution est justement une question centrale dans une activité de publication, "Et si Internet nous a appris quelque chose, c'est bien que la puissance combinée des nœuds individuels (et se renforçant mutuellement) est illimitée" (p. 163).
Internet est plus qu'un nouveau canal de diffusion – comme l'ont été le télégraphe, la radio ou la télévision –, c'est aussi une "infrastructure" (p. 173) et un moyen d'imaginer de nouvelles modalités de distribution.

## 1.1.3. Vers une hybridation
Dans la conclusion de son ouvrage Alessandro Ludovico revient sur l'évolution des medias, et il note que la situation de l'imprimé est bien distincte de celles de la musique ou de la vidéo : "L'impression, néanmoins, est un cas très différent, puisque le medium – la page imprimée – est plus qu'un simple vecteur de matériel destiné à être montré sur un système d'affichage distinct ; c'est aussi le système d'affichage lui-même." (p. 175)
Ainsi, passer au numérique impose une modification de nos usages et de notre rapport avec le livre.
L'*hybridation* serait le moyen de dépasser ce qui se limite à des nouveaux modes de lecture ou de consommation, pour atteindre un niveau "processuel" : le développement de nouvelles formes artistiques, des incidences sur l'environnement social et politique, etc.

L'"impression postnumérique", concept clé de ce livre, serait donc la combinaison de plusieurs éléments : depuis le modèle de la souscription via Internet jusqu'à l'hybridation de l'imprimé et du numérique en passant par l'utilisation d'un ordinateur pour concevoir et produire les objets imprimés et numériques.
Citons quelques exemples utilisés par Alessandro Ludovico.
Cory Doctorow propose des formats numériques gratuits (PDF, EPUB ou audio) et imagine plusieurs formes de livres imprimés – du format poche imprimé à la demande via la plate-forme Lulu.com à la version plus luxueuse fabriquée par des artisans du livre en nombre limité –, et des façons originales d'y accéder.
Le livre numérique peut devenir un produit d'appel, gratuit, pour déclencher des ventes de livres imprimés.
Le projet de Tim Devin d'impression de courriers électroniques distribués sur des pare-brise de voitures dans différentes villes des États-Unis est un exemple de la matérialisation d'objets numériques : un simple mail peut retrouver une figure d'autorité dès qu'il y a impression.
Enfin, certaines publications imprimées ont évolué vers le numérique, c'est le cas de *Boing Boing* : ce fanzine (imprimé) connaît un succès important à la fin des années 1980 avant de cesser sa publication faute de distributeur ; une version web est alors créée quelques années plus tard, considérées comme un véritable pionnier du phénomène des blogs.

*Post-Digital Print* se referme sur la condition de cette hybridation : pour Alessandro Ludovico la métamorphose de l'édition est possible par le dépassement de toute "affiliation idéologique" (p. 180) liée aux medias imprimés ou numériques.

*Post-Digital Print* est un cas à part dans le domaine des ouvrages sur l'édition ou la publication numérique, cas qui pourrait se rapprocher de *6|5* d'Alexandre Laumonier – consacré au trading à haute fréquence sous le prisme du développement des moyens de communication.
Pour mieux comprendre et étudier un sujet il faut bien évidemment analyser et interroger son progrès historique, ainsi Alessandro Ludovico retrace l'évolution technique de l'édition en illustrant ses propos de descriptions d'expérimentations souvent méconnues : des procédés comme la machine miméographique facilitant la reproduction d'imprimés clandestins ou des premières revues de science-fiction ; la mise en réseau permettant une distribution plus large avec le projet *Whole Earth Catalog* ; la technique d'impression offset avec l'apparition de revues comme *Village Voice* ou *San Francisco Oracle* aux mises en page plus audacieuses ; la photocopieuse amorçant le mouvement des fanzines ; ou le passage au tout numérique pour certains journaux à partir de l'année 2008.

Parmi les expérimentations que nous pouvons observer depuis 2012 à la suite de l'*écriture* de cet essai, voici quelques exemples choisis de façon subjective et reflétant les propos d'Alessandro Ludovico :

- la réédition de *Flatland* d'Edwin A. Abbott par Epilogue {% cite kehe_one-man_2016 %} combinant un objet imprimé de grande qualité et un programme générant des formes en rapport avec le livre ;
- *Zeitgeist* de Frank Adebiaye {% cite fauchie_zeitgeist_2013 %} offrant deux lectures complémentaires, dont une numérique et aléatoire ;
- *La dette technique* de Bastien Jaillot et *Design d'icônes : le manuel* de Sébastien Desbenoit aux éditions Le train de 13h37 associant tous les deux la souscription *avant* publication et des versions imprimée et numérique ;
- *Atomic Design* de Brad Frost {% cite frost_atomic_2016 %} donnant la possibilité de suivre l'écriture du livre et d'y contribuer ;
- la revue *From–To* de l’ÉSAD Grenoble Valence {% cite fauchie__2015 %} avec un site web librement accessible et mis à jour régulièrement, et un livre imprimé au format et au prix réduits.

C'est avec une grande acuité que cet essai donne un aperçu clair de ce qui a été entrepris, et une perspective ouverte du territoire à explorer.
*Post-Digital Print* parvient à articuler plusieurs axes déterminants pour l'édition, cette volonté d'embrasser la multitude des mutations de l'édition est relativement rare dans les ouvrages théoriques de ce domaine, généralement orientés vers une problématique principale – que ce soit *Après le livre* de François Bon, *Le livre à l'heure numérique* de Françoise Benhamou, *Read/Write Book : Le livre inscriptible*, ou encore *Pratiques de l'édition numérique* de Michael Sinatra et Marcello Vitali-Rosati.
Alessandro Ludovico traite autant des aspects de design et d'usage, des enjeux de diffusion et de distribution, de la mise en réseau des acteurs de l'édition, de la recherche de stratégies économiques originales, et enfin des démarches d'archivage et d'accès.
Lire et partager *Post-Digital Print* ouvre des perspectives pour la publication d'aujourd'hui et de demain.

## 1.1.4. Les chaînes de publication comme moyen d'une hybridation
Les réflexions d'Alessandro Ludovico nous permettent d'entrevoir un des éléments qui pourrait faire advenir cette *hybridation*.
Un des enjeux de la mutation du texte – plutôt que du livre – se situe au niveau des moyens de conception et de production.
La chaîne de publication, sujet peu abordé de façon frontale par Alessandro Ludovico, serait l'un des points de convergence des différentes dynamiques présentées dans *Post-Digital Print* : mutation de l'imprimé plutôt que fin du papier, expérimentations éditoriales grâce aux améliorations techniques, transformation du papier, édition numérique, archivage et réseau.
Si l'hybridation est la réalisation de l'"impression postnumérique" (p. 178), comment cette hybridation peut-elle être réalisée ?
Penser une chaîne de publication libérée d'"affiliation idéologique" (p. 179), capable de générer des formes et des formats tant pour le papier que pour le numérique, et en capacité de nourrir des ressources connectées, serait une solution.

Une chaîne de publication inspirée du web {% cite fauchie_chaine_2017 %} correspond à un ensemble modulaire de technologies et de méthodes issues du développement web, et permet de se passer du duo classique de l'édition : traitement de texte et logiciel de publication assistée par ordinateur.
Il est trop tôt pour développer précisément ce concept, mais nous pouvons déjà le décrire : il s'agit de l'association d'un langage de balisage léger permettant de se passer de logiciel particulier pour écrire et structurer des contenus, d'un système de contrôle de version pour travailler à plusieurs sur un texte, et de la génération de formats multiples – du PDF pour l'impression à l'EPUB en passant par du HTML pour une version web.
Cette chaîne est l'outil qui concrétise, ou tout du moins ouvre la possibilité de concrétiser la thèse d'Alessandro Ludovico.
Une modularité ne restreint pas la chaîne d'édition à la génération d'un seul format.
Des technologies ouvertes limitent l'"affiliation idéologique" de l'outil, et suppriment la dépendance à des logiciels en particulier – eux aussi porteurs d'idéologie, les traitements de texte et les outils de publication assistée par ordinateur sont avant tout pensés pour l'imprimé.
Les formats utilisés dans ce workflow assurent une pérennité et un archivage des productions.
La gestion des fichiers et des échanges avec un système de contrôle de version est une mise en réseau dès la conception et la production.

Loin de nous l'idée de détourner la thèse d'Alessandro Ludovico, les chaînes de publication ne sont qu'un point soulevé indirectement par son ouvrage, mais aujourd'hui il s'agit d'un nœud déterminant pour le texte, le livre, l'action de publier.
L'auteur de *Post-Digital Print* pose un certain nombre de faits marquants et analyse des mutations essentielles de l'imprimé, il esquisse et même établit les évolutions possibles du texte.
