---
title: "1.3. Progrès technique"
category: 1. Mutations du livre
order: 4
partiep_link: /1-mutations-du-livre/1-2-solutionnisme/
parties_link: /2-experimentations/2-0-introduction/
repo: _memoire/1-mutations-du-livre/-1-3-progres-technique.md
---
{% include_relative -1-3-progres-technique.md %}
