Ce document, que vous lisiez la version web, la version PDF ou la version imprimée, est le mémoire d'Antoine Fauchié sous la direction d'Anthony Masure et de Marcello Vitali-Rosati.
Il s'agit d'un travail de recherche réalisé dans le cadre d'une Validation des acquis de l'expérience (VAE) en Master Sciences de l'information et des bibliothèques, spécialité Publication numérique à l'[Enssib](http://www.enssib.fr/).

## Soutenance

La soutenance de ce mémoire aura lieu **vendredi 28 septembre 2018 à l'Enssib**, [17/21 boulevard du 11 novembre 1918](https://www.openstreetmap.org/way/446223812#map=18/45.77982/4.86301), salle 100.
La soutenance est ouverte au public, si vous souhaitez assister à cette soutenance, merci par avance de compléter le formulaire ci-dessous :

<form name="contact" method="POST" netlify>
  <p>
    <label>Prénom :</label>
    <input type="text" name="prenom" />
    <label>Nom :</label>
    <input type="text" name="nom" />
  </p>
  <p>
    <label>Adresse électronique :</label>
    <input type="email" name="email" />
  </p>
  <p>
    <label>Je viens à la soutenance du vendredi 28 septembre 2018 à 15h30 :</label>
    <select name="role[]" required>
      <option value="oui">Oui</option>
      <option value="non">Non</option>
    </select>
  </p>
  <p>
    <label>Vous pouvez ajouter un message :</label>
    <textarea name="message"></textarea>
  </p>
  <p>
    <button type="submit">Valider</button>
  </p>
</form>

## Version
Version {{ site.version }}, datée du {{ site.versiondate }}.

Site web : [https://memoire.quaternum.net](https://memoire.quaternum.net)

[Version PDF imprimable](https://memoire.quaternum.net/telechargement/fauchie-antoine-vers-un-systeme-modulaire-de-publication-cc-by-nc-sa.pdf).


## Licence
Licence Creative Commons BY-NC-SA, Attribution-NonCommercial-ShareAlike 4.0 International.
Vous êtes libre d'utiliser et de modifier les textes de ce mémoire à condition de me citer systématiquement, de ne pas faire d'usage commercial de votre utilisation, et de partager votre version dans les mêmes conditions indiquées ici, donc avec la même licence.

## Fabrication
Ce mémoire est fabriqué avec les langages, composants, programmes et logiciels suivants :

* langage de balisage léger Markdown ;
* générateur de site statique Jekyll ;
* extensions pour Jekyll : jekyll-scholar et jekyll-microtypo ;
* système de gestion de versions Git ;
* plate-forme d'hébergement de dépôt Git GitLab ;
* déploiement continu, hébergement et CDN : Netlify ;
* script paged.js pour la conversion HTML > PDF ;
* éditeur de texte et IDE : Atom ;
* navigateur web : Mozilla Firefox.

Ce mémoire a été écrit avec ♥ et quelques gouttes de sueur en écoutant la playlist suivante :

- [Lebanon Hanover - Gallowdance](https://www.youtube.com/watch?v=WPw7nlluRdc)
- [Shannon Wright - With Closed Eyes](https://www.youtube.com/watch?v=hfCYG0Jkq1Y)
- [Human Tetris - Things I Don't Need](https://www.youtube.com/watch?v=ALk3o7m5Jt8)
- [Battles - Futura](https://www.youtube.com/watch?v=9zAJ0R9zHFY)
- [girlpool - Soup](https://girlpoool.bandcamp.com/track/soup)
- [Swans - Bring the Sun](https://www.youtube.com/watch?v=vlONStHIBUA)
- [Franz Schubert - La mort et la jeune fille](https://www.youtube.com/watch?v=qXhxi4z0bLs)
- [Cigarettes After Sex - Dreaming of You](https://www.youtube.com/watch?v=ZuIDh4XIzxU)
- [Shellac - Cooper](https://www.youtube.com/watch?v=t_et94mhU9s)
- [Andy Shauf - Wendell Walker (Live at The Drake Hotel)](https://www.youtube.com/watch?v=3dThrMKG4r4)
- [Mogwai - Olds Poisons](https://www.youtube.com/watch?v=DK6yw2xQ4y4)
- [Mix - Modular ambient](https://www.youtube.com/watch?v=gOqCXuUtsbg&start_radio=1&list=RDgOqCXuUtsbg)
- [r beny - Spring in Blue](https://www.youtube.com/watch?v=0we7tkr-jh4)
