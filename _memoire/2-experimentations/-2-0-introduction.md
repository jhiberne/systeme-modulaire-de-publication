Les textes d'Alessandro Ludovico, d'Evgeny Morozov et de Gilbert Simondon constituent des fondations pour envisager une nouvelle relation au numérique, à la technologie et à la technique.
Et plus encore lorsque nous parlons de la façon dont nous pouvons concevoir, fabriquer et produire des livres.
En nous appuyant sur la technique en tant que logiciels, programmes, méthodes et organisation, nous pouvons considérer de nouveaux modèles ouverts permettant de faire _avec_ le numérique.
Nous devons désormais ajouter une nouvelle dimension à ce moment théorique, celle de la pratique : comment des concepts peuvent être appliqués dans le champ de l'édition ?

Après ces trois commentaires de textes longs formant la première partie, nous analysons des cas emblématiques venant confirmer ou tempérer les premières hypothèses de ce mémoire.
Depuis les méthodes et outils mis en place par la maison d'édition américaine O'Reilly jusqu'à la démarche singulière de la revue _Distill_, en passant par l'innovant _workflow_ de Getty Publications, voici trois analyses de cas qui nous donne la matière nécessaire à l'établissement des principes exposés dans la troisième partie.

O'Reilly Media, une structure d'édition spécialisée dans des ouvrages sur l'informatique, utilise depuis de nombreuses années un certain nombre de méthodes et d'outils pour faciliter le travail des auteurs et gagner en efficacité.
Cela se traduit notamment par l'adoption de standards et la réalisation d'une plate-forme sur mesure.

Getty Publications, la maison d'édition de la fondation The Getty, crée une chaîne d'édition d'un nouveau genre.
Constatant la nécessité de repenser les éditions numériques de leurs catalogues, la mise en œuvre de ce système de publication a une incidence sur la façon, plus globale, de faire des livres, quelle que soit leur forme.

_Distill_ est une revue scientifique dans le domaine du _machine learning_, uniquement en version numérique, intégrant des schémas interactifs et versionnant les sources des articles.
Cette revue répond à des besoins très contemporains – en terme d'accès ou de fonctionnement – tout en posant un certain nombre de questions dans le champ de la publication académique.

Sans ces réalisation ou expérimentations, ce mémoire n'aurait pas lieu d'être : nous ne faisons ici que détailler des initiatives pionnières et, d'une certaine façon, théoriser des applications pratiques qui répondent à des besoins variés.
Comme pour la première partie, ces matériaux font partie intégrante du mémoire et ne sont pas placés en annexe, proposant ainsi une forme originale.
Ces analyses de cas, avec les textes étudiés en première partie, représentent le socle d'une réflexion développée dans la troisième partie.
