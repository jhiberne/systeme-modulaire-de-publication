Le livre connaît des évolutions constantes, depuis une quarantaine d'années l'informatique puis le numérique apportent de nouvelles configurations : une informatisation des métiers du livre avec le développement de logiciels spécifiques, et des nouveaux modes d'accès notamment avec l'_ebook_.
Ces mutations, récentes en ce qui concerne le livre numérique, nous amènent à interroger la façon dont les publications sont conçues et produites, jusqu'alors centrées sur la version imprimée.
La chaîne d'édition, considérant qu'il s'agit de l'ensemble des outils permettant de fabriquer un livre, est impactée par les évolutions techniques que sont par exemple le traitement de texte, le logiciel de publication assistée par ordinateur, le format EPUB ou encore le livre web.
En prenant en compte ces bouleversements tant en terme de processus de construction des publications que de méthode de travail, nous avons défini un nouveau modèle alternatif.
Avant de réexaminer ce système et surtout de le questionner, nous revenons sur le cheminement qui nous a conduit à cette approche originale.

## Les mutations du livre et des expérimentations inédites
Les trois textes que sont _Post-Digital Print : la mutation de l'édition depuis 1894_ d'Alessandro Ludovico, _Pour tout résoudre, cliquez ici : l’aberration du solutionnisme technologique_ d'Evgeny Morozov et _Du mode d'existence des objets techniques_ de Gilbert Simondon nous ont apporté trois points essentiels pour analyser les évolutions du livre et notre rapport à la technologie ou à la technique sous l'angle de la publication.

La question de l'hybridation de l'imprimé et du numérique est abondamment traitée par Alessandro Ludovico, les possibilités de croisement de ces deux mediums sont nombreuses, ne conduisant non pas à la mort du papier, mais plutôt à des formes multiples et complémentaires.
La condition de l'hybridation d'une publication dépend en partie des possibilités de sa fabrication : générer facilement des versions numériques – livre numérique et livre web par exemple – aux côtés de plusieurs variantes imprimées – un tirage limité et une impression à la demande par exemple – dépend des méthodes et des outils mis en place.

Depuis l'avènement des logiciels d'édition, le livre connaît une forme de solutionnisme technologique : l'informatique est privilégiée pour solutionner des problèmes techniques et humains, que ce soit la fabrication de l'objet imprimé ou numérique, ou le travail autour du texte rassemblant plusieurs métiers différents.
L'ouvrage d'Evgeny Morozov est une critique du solutionnisme technologique qui consiste à recourir à des solutions techniques sans examiner le fondement du problème rencontré.
Il en est de même dans le cadre de processus de production d'un texte : les tâches à réaliser pour construire un ouvrage ne peuvent se limiter à des fonctionnalités d'un logiciel, aussi puissant soit-il.
D'autres solutions peuvent être mises en place comme le détournement des formats du Web pour produire une publication, donnant l'occasion de questionner l'usage de la technologie.

Au-delà de l'utilisation des outils numériques, la question du rapport de l'homme à cet environnement technique est essentielle, nous devons sortir du choix binaire qu'est sa considération uniquement utilitariste ou sa sacralisation.
Gilbert Simondon propose de reconsidérer la technique et d'attribuer à la machine une individuation : l'objet technique est doté d'une synergie et d'une certaine indépendance dans son fonctionnement, contrairement à des objets non techniques comme les ustensiles.
Ainsi l'homme ne doit plus se positionner _dans_ la machine mais au-dessus ou au-dessous, il n'est plus l'opérateur mais le chef d'orchestre.
Les outils utilisés classiquement dans l'édition sont peu ouverts et placent l'homme dans une position d'aliénation, repenser la chaîne de publication permet de modifier notre rapport aux machines, et donc, par extension, aux programmes.

À la suite de ces trois analyses nous avons exposé trois démarches qui tendent à repenser la façon de fabriquer des publications.
Tout d'abord O'Reilly Media a développé des méthodes et une plate-forme, afin de faciliter la production de versions imprimées et numériques, et pour permettre aux auteurs d'écrire dans de meilleures conditions.
S'il s'agit d'une recherche de perfectionnement guidée par la rentabilité, O'Reilly Media a mis en place et participe encore aujourd'hui à un format et à un langage de balisage léger, ouverts, apportant une certaine réflexivité dans l'acte d'écriture et d'édition, en plus d'apporter des innovations techniques – il s'agit des formats DocBook et AsciiDoc.
Getty Publications a probablement bénéficié de ces réflexions pour construire une chaîne originale, grandement basée sur les technologies du Web, générant autant une version web, un livre numérique au format EPUB ou des fichiers PDF pour différents types d'impression.
Enfin la revue _Distill_ bouleverse les modes de gestion d'un texte dans un cadre académique et scientifique, s'appuyant sur des méthodes issues du développement informatique comme le versionnement, et invitant les auteurs à participer au processus d'édition.

Ces analyses de texte nous donnent un cadre de réflexion nécessaire, accompagné de présentations de cas qui illustrent nos recherches, la chaîne de publication Quire de Getty Publications est à ce titre particulièrement emblématique des mutations en cours.
Nous devons souligner que ces exemples ne sont pas exempts de critiques quand à leur contexte et à leur choix de réalisation, tout comme les propositions qui suivent.

## Un système modulaire
Décomposer le processus de publication en définissant ses étapes constitue les prémisses incontournables à l'exposition d'un nouveau modèle.
Écrire et structurer ; partager, collaborer et valider ; mettre en forme ; générer des formats et publier : ces différentes phases représentent chacune un certain nombre de défis, que les outils et les méthodes utilisés actuellement ne parviennent qu'à résoudre partiellement – nous pouvons illustrer ce point par la complexité de la production des livres numériques, tâche souvent sous-traitée.
Les traitements de texte et les logiciels de publication assistée par ordinateur engendrent même des confusions et un certain nombre d'erreurs en appliquant un modèle monolithique composé d'étapes irréversibles.
Le passage d'une _chaîne_ de publication à un _système_ modulaire vise à désolidariser les phases de publication pour qu'elles constituent une synergie plutôt qu'une suite linéaire, mais également à clarifier la confusion entre structure et mise en forme, à favoriser une compatibilité, à permettre la continuité entre les étapes et l'indépendance vis-à vis de certains logiciels, ou enfin à donner aux auteurs et aux éditeurs les moyens de se réapproprier leurs outils de création et de production.
Nous parlons ici d'un système en particulier, sans qu'il s'agisse pour aurtant _du_ système.
À travers les principes exposés dans ce texte nous souhaitons engager une dynamique ouverte, afin qu'elle donne lieu à d'autres propositions.

Grâce à la présentation des phases du processus d'édition, et en s'appuyant sur les exemples présentés dans la seconde partie, nous avons été en mesure d'exposer les trois principes qui constituent un nouveau modèle de chaîne de publication.
L'**interopérabilité** est la condition pour faciliter la circulation de l'information et offrir une compatibilité entre les différents outils, son application peut se traduire par l'usage de langages de balisage léger pour inscrire et structurer le texte.
La **modularité**, qui consiste en l'assemblage de fonctions sans dépendance à un outil en particulier, est conditionnée par l'interopérabilité.
La chaîne de publication Quire est l'application de ce principe : un générateur de site statique produit des fichiers HTML qui sont la base d'un site web et qui sont transformés en PDF par un autre processeur.
Un autre module peut être ajouté pour produire un format EPUB, le format du livre numérique.
Enfin, la **multiformité** est la capacité de produire des formes diverses d'une même publication : l'incontournable version imprimée, le format EPUB désormais courant, mais également un site web ou des jeux de données dans des cas spécifiques.
Cette multiformité dépend de la modularité qui dépend elle-même de l'interopérabilité, ce trio est interdépendant et forme un système de publication.
Il est temps d'abandonner les traitements de texte et les logiciels de publication assistée par ordinateur {% cite attwell_i_2017 %}.

Loin de résoudre automatiquement tous les problèmes énoncés dans la partie 3.1., ce système modulaire présente de nouvelles contraintes par rapport aux chaînes d'édition conventionnelles.
L'acquisition d'une culture technique plutôt que la connaissance de fonctionnalités, la maîtrise de langages plutôt que celle de logiciels métiers, une priorité sur les artefacts numériques plutôt que sur l'ouvrage imprimé, l'acceptation d'une typographie sous forme de flux pour les versions numériques, les défis sont nouveaux par rapport aux approches dites classiques.
Il s'agit, d'une certaine façon, du coût du changement de paradigme imposé par ce _système_, que nous pouvons qualifier de _nouvelles dépendances_.

## Pour de belles dépendances
Le schéma traditionnel basé sur un traitement de texte et un logiciel de publication assistée par ordinateur est relativement simple à maintenir, abstraction faite de la dépendance à des entreprises privées qui développent et vendent ces logiciels et proposent des mises à jour – payantes ou inclues dans des abonnements.
Ici, avec un système constitué de briques reliées entre elles – souvent _open source_ ou libre –, les dépendances techniques sont nouvelles : si un module n'est plus actualisé par une personne ou une communauté, il faut trouver une alternative ou envisager un développement pour palier au défaut de mise à jour.
Par ailleurs, les dépendances peuvent produire une réaction en chaîne : un générateur de site statique fonctionne par exemple avec un langage de programmation particulier, qui n'est pas forcément installé par défaut sur tous les systèmes d'exploitation.
Un changement de système d'exploitation peut provoquer un _effet domino_, contournable grâce à la dimension modulaire du système : un nouveau générateur de site statique pourrait venir remplacer celui choisi initialement.

Ces dépendances font appel à des humains, des individus, des collectifs, des communautés ou des entreprises : ils œuvrent indépendamment ou en réseau pour maintenir différents projets.
Contrairement au cas du logiciel qui est développé par une entreprise, ici l'intelligence collective est non centralisée : les acteurs des projets sont distribués ou peuvent l'être grâce au caractère libre du projet.
Ces initiatives deviennent aujourd'hui fragiles, certaines briques technologiques ne devant parfois leur existence qu'à des individus isolés {% cite eghbal_sur_2017 %}, elles démontrent pourtant la puissance d'une architecture du savoir distribuée, ouverte et évolutive.

Nous désignons les contraintes exposées dans la dernière partie et dans cette conclusion comme de _belles dépendances_ : la capacité d'un système modulaire de publication de fonctionner dans un temps long repose sur des compétences, et sur des démarches bien souvent artisanales.
Plutôt que de faire porter la continuité d'une chaîne d'édition sur la maintenabilité d'un ou plusieurs logiciels par des sociétés privées ou des communautés centralisées, un système modulaire repose sur l'assemblage de plusieurs programmes, autonomes et connectés.
La dimension artisanale correspond à ces nombreux projets conçus chacun par une poignée d'individus pour satisfaire des exigences particulières.
Ce mémoire est par exemple construit grâce à l'association une quinzaine de programmes différents, certains étant élaboré par une ou deux personnes [^annexe-back].
Réunir ces composants ne signifie pas être développeur, mais suppose de s'immerger dans le fonctionnement de fabrication d'un livre, de _faire_.

>Nous avons trop peu d'occasions de vraiment _faire_ quoi que ce soit parce que notre environnement est trop souvent prédéterminé à distance.  
{% cite crawford_eloge_2016 -l 84 %}

Les _belles dépendances_ sont celles des programmes et de leur réunion, c'est une nouvelle configuration qui remet en cause l'usage de fonctions préconçues propres aux chaînes d'édition classiques.
Mais la transformation qui a lieu n'est pas qu'une question de choix techniques, l'utilisation d'un système modulaire nécessite l'acquisition d'une culture technique, voir l'intégration d'un profil de _designer_/développeur dans l'équipe éditoriale.
L'acte d'éditer est lui-même révolutionné : "Le « numérique » n’a donc pas changé seulement les techniques éditoriales, mais, plus généralement, le sens même de l’édition." {% cite epron_ledition_2018 %}
Il s'agit du dernier aspect que nous souhaitons aborder, éditer un ouvrage entraîne la conception du système de publication, en fonction du type de publication et des contraintes inhérentes à la structure ou à la diffusion.
Cela a un coût, mais la question ici est plutôt de construire, de maîtriser et de faire soi-même évoluer une chaîne de publication, cette activité devenant toute différente, "elle est une autre expérience du temps" {% cite crawford_eloge_2016 -l 68 %}.

Un système modulaire de publication implique un engagement important.
La réappropriation des processus d'écriture par les auteurs et les éditeurs devient une urgence.
Ne plus être seulement l'opérateur de ses outils d'édition, ou y être assujetti, requiert un certain degré d'ouverture et la découverte de nouvelles connaissances.
À l'heure où la technologie est partout présente, et de plus en plus sans avoir accès à ses rouages ou sans pouvoir y interférer, la perspective d'une reprise en main est stimulante, surtout si le prix à payer est un apprentissage.

[^annexe-back]: Voir l'annexe Versions du mémoire pour en savoir plus sur la construction du document, et plus particulièrement des versions web et imprimée.
