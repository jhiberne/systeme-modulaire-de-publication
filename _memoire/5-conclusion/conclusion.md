---
title: "Pour de nouvelles dépendances"
category: Conclusion
order: 1
redirect_from:
  - /5-conclusion/
partiep_link: /3-un-systeme-modulaire/3-2-les-principes-d-un-nouveau-modele-de-publication/
repo: _memoire/5-conclusion/-conclusion.md
---
{% include_relative -conclusion.md %}
