Interroger et critiquer la technologie et plus spécifiquement les chaînes de publication classiques à l'aide de textes théoriques, puis présenter plusieurs initiatives de modèles alternatifs en ce qui concerne la fabrication de livres ou de publications, voilà l'objectif des deux premières parties de ce travail de recherche.
Avant de fixer les étapes d'un processus de publication et de proposer les principes d'un nouveau modèle, revenons sur ces deux moments.

L'analyse de _Post-Digital Print_ d'Alessandro Ludovico nous a permis de prendre la mesure de l'hybridation entre le "papier" et le "numérique".
Ces deux formes ne sont pas en opposition, des démarches éditoriales enthousiasmantes mêlant des supports imprimés et des versions numériques et présentées dans cet ouvrage le prouvent avec beaucoup d'acuité.
Pour concevoir et produire ces formes plurielles il y a une nécessité de repenser les chaînes de publication, habituellement conçues pour générer des formes imprimées dont résultent des objets numériques.

Les pratiques d'édition, quelles produisent des publications imprimées ou des _ebooks_, dépendent de logiciels que l'on peut qualifier de "boîtes noires" : difficile de comprendre leur fonctionnement ou d'influer sur leur évolution.
C'est, d'une certaine façon, ce qu'Evgeny Morozov nomme le "solutionnisme technologique", c'est-à-dire l'utilisation systématique de la technologie pour régler un problème sans forcément bien délimiter celui-ci.
_Pour tout résoudre cliquez ici_ apporte une critique bienvenue des usages de la technologie dans le champ social – au sens large.
À partir de cela nous pouvons reconsidérer les chaînes de publication classiques pour développer de nouveaux modèles plus enclins à répondre à des contraintes bien identifiées.

Le positionnement par rapport à la technologie ou plus globalement à la technique est le sujet du texte de Gilbert Simondon, _Du mode d'existence des objets techniques_.
La mauvaise reconnaissance de la technique induit notamment une aliénation du monde contemporain, il s'agit de mieux définir ce qu'est un "objet technique" et de développer une culture sur cette base.
Ainsi, l'être humain ne doit plus être _dans_ la machine, mais à ses côtés : au-dessous, dans la gestion d'éléments qui composent la machine, et au-dessus, dans l'orchestration de l'ensemble technique que forment ces éléments.
Le progrès technique n'est alors plus cette recherche effrénée – aveugle ? – de l'innovation continue mais plutôt une évolution irrégulière et une émancipation de l'homme.
Concevoir un modèle de publication dans ces conditions peut se traduire par l'utilisation des méthodes et outils du développement web, ouverts et laissant une grande place à l'humain.

Trois analyses de cas sont venues confirmer cette triple approche théorique, la première concerne le _workflow_ de publication d'O'Reilly Media.
Cette structure d'édition spécialisée dans l'informatique utilise des technologies issues de ce domaine pour fabriquer ses livres, qu'ils soient imprimés ou numériques.
L'intégration du format XML, puis d'un langage de balisage léger comme AsciiDoc, a permis une meilleure prise en compte des besoins des auteurs.
Par la suite cela se traduit également par la création d'une plate-forme intégrant ces technologies ainsi qu'un processus de versionnement.

Plus spécifique encore dans l'utilisation des procédés issus de l'informatique, Getty Publications détourne des dispositifs du développement web pour constituer un système de publication numérique.
Le traitement de texte et le logiciel de publication assistée par ordinateur sont remplacés par un générateur de site statique produisant des fichiers HTML à partir de sources en Markdown ou YAML.
Ces fichiers HTML produisent directement un site web, ou sont ensuite transformés en livre numérique ou en PDF destiné à l'impression.
Cette manière de produire des publications est singulière, et connaît quelques exemples similaires dans le domaine de la documentation ou des manuels.

Enfin, la jeune revue _Distill_ créée en 2017 propose des articles en version numérique et incluant des schémas interactifs, ce qui représente une plus value dans le champ de l'intelligence artificielle – les _papiers_ au format PDF ne suffisant pas à comprendre des concepts complexes.
En plus de placer le lecteur au même niveau que le chercheur – en lui faisant manipuler des données –, _Distill_ met à disposition les sources des articles sur la plate-forme GitHub.
Le versionnement est ainsi une composante du fonctionnement de la revue, jusque dans les conditions de soumission des articles – chaque proposition doit reprendre le modèle de la revue dans un dépôt dédié.

Nous disposons de critiques théoriques et de cas pratiques, mais sur quels principes reposent ces initiatives innovantes que peuvent être la chaîne de publication Quire ou la revue _Distill_ ?
N'y a-t-il pas, derrière les choix techniques, des fondements communs permettant de reproduire un système de publication non conventionnel ?
Cette troisième partie a pour objectif d'extraire les axiomes qui constituent ces projets originaux, pour les structurer, les organiser et les présenter.
Avant cela, nous exposons les différentes étapes d'une chaîne de publication, afin de bien identifier les contraintes qui devront être prises en compte dans les principes proposés.
Cet exposé distingue les phases d'une activité d'édition ou de publication, telles que l'inscription d'un texte ou la mise en forme, tout en soulignant les implications et les questionnements.
