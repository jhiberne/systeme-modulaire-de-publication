---
title: "3.1. Les étapes d'une chaîne de publication"
category: 3. Un système modulaire
order: 2
partiep_link: /3-un-systeme-modulaire/3-0-introduction/
parties_link: /3-un-systeme-modulaire/3-2-les-principes-d-un-nouveau-modele-de-publication/
repo: _memoire/3-un-systeme-modulaire/-3-1-les-etapes-d-une-chaine-de-publication.md
---
{% include_relative -3-1-les-etapes-d-une-chaine-de-publication.md %}
