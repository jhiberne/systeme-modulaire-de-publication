# Journal

## Mardi 29 mai 2018
Ouverture publique du dépôt, mais sans annonce pour le moment.

## Vendredi 18 mai 2018
Reprise des travaux, enfin, après 6 mois de coupure... Organisation des semaines qui viennent pour terminer la rédaction.

## Jeudi 2 novembre 2017
Problème lors de la suppression d'une branche déjà fusionnée, pour voir d'où vient le problème : `git show nom-de-la-branche`.

## Samedi 8 juillet 2017
Fin du commentaire de *Pour tout résoudre cliquez ici*, et renommage des branches du dépôt Git pour faire le lien avec les *issues* correspondantes. Début de la (re)lecture de *Post-Digital Print* d'Alessandro Ludovico.

## Jeudi 6 juillet 2017
Fin de la lecture de *Pour tout résoudre cliquez ici*, je commence le commentaire de texte. Deux notes : la longue prise de notes me permet d'écrire relativement vite le commentaire de texte ; le bruit des cloches des moutons me rappelle celui des glaçons dans les verres de pastis, un dépaysement en vaut un autre.

## Mercredi 5 juillet 2017
J'ai enfin commencé à faire une bibliographie sérieuse, disponible sur Zotero, mais encore en cours de réalisation : [https://www.zotero.org/antoinentl/items/collectionKey/6FF5K5PS](https://www.zotero.org/antoinentl/items/collectionKey/6FF5K5PS)

## Lundi 2 juillet 2017
Je voulais trouvé une solution pour afficher facilement l'historique Git, l'idée est d'avoir une vision globale de l'avancée des travaux d'un certain point de vue. Voici la commande pour afficher l'historique des commits de toutes les branches :  
`git log --branches --remotes --tags --graph --oneline --decorate --pretty=format:"%h - %ar - %s"`

## Samedi 1er juillet 2017
Rendez-vous avec Marcello Vitali-Rosati – l'un de mes deux directeurs de mémoire –, je suis reparti avec beaucoup de références, et pas mal d'idées suite à des échanges très enrichissants.

## Vendredi 30 juin 2017
Après une pause de dix jours (prestations en attente, projets à finaliser, etc.), je reprends la lecture de Morozov.

## Jeudi 29 juin 2017
Strabic a publié mon article sur le workshop PrePostPrint d'avril 2017 : [Workshop PrePostPrint - Chercher, manipuler, partager, imprimer](http://strabic.fr/Workshop-PrePostPrint) ! Joie !

## Vendredi 16 juin 2017
Rédaction de l'analyse du workshop PrePostPrint, qui est également une proposition d'article pour la revue en ligne [Strabic](http://strabic.fr/) : réaliser ce type de travail sous contraintes (principalement de temps) est une très bonne chose ! Il faut que je me fixe plus de contraintes !

## Mercredi 14 juin 2017
Finalisation de la fiche synthétique de *Pour tout résoudre cliquez ici. L'aberration du solutionnisme technologique* d'Evgeny Morozov, je vais tout de même prolonger ma lecture, et notamment le dernier chapitre.

## Lundi 12 juin 2017
Fin de la lecture (active) des deux premiers chapitres de *Pour tout résoudre cliquez ici. L'aberration du solutionnisme technologique* d'Evgeny Morozov, lecture qui est beaucoup plus rapide que celle de *Du mode d'existence des objets techniques* de Gilbert Simondon, mais également moins riche.

## Mercredi 7 juin 2017
Point rapide avec Marcello Vitali-Rosati – l'un de mes deux directeurs de mémoire –, aussi rapide que motivant.

## Mardi 6 juin 2017
Début de la lecture de *Pour tout résoudre cliquez ici. L'aberration du solutionnisme technologique* d'Evgeny Morozov.

## Samedi 3 juin 2017
Premiers retours d'Anthony Masure – l'un de mes deux directeurs de mémoire – via une *merge request* : cela fonctionne bien, même si ce n'est pas encore l'idéal. Retours par ailleurs très engageants – point de vue très subjectif.

## Vendredi 2 juin 2017
Deux heures de configuration de mes dossiers et de mon dépôt, avec beaucoup de tests. Git requiert une certaine discipline, assez intuitive, il ne faut pas trop se poser de questions, en fait.

## Jeudi 1er juin 2017
Longue discussion avec Frank à propos de Git et de biens d'autres sujets. Rédaction de quelques lignes de documentations sur l'usage de Git pour ce mémoire.

## Mardi 30 mai 2017
Beaucoup plus de recherches que prévu pour O'Reilly Media : il s'agit bien de voir et d'expliquer la logique complète de la démarche de publication, plutôt que de se concentrer uniquement sur un format ou une plateforme.  
Recherches autour de git : il faut que je fonctionne autrement, par branches, je change donc mon workflow.

## Lundi 29 mai 2017
Nouvelle analyse d'objet : les outils de publication d'O'Reilly Media !

## Vendredi 26 mai 2017
Finalisation du commentaire de texte *Du mode d'existence des objets techniques* ! Conclusion : lecture beaucoup plus longue que prévue, mais rédaction *relativement* rapide. Le plus complexe est finalement de synthétiser la pensée de l'auteur, ou tout du moins l'expliquer.

## Lundi 15 mai 2017
Début de l'analyse de la chaîne de publication numérique de Getty Publications !

## Dimanche 14 mai 2017
Après quelques semaines légères, je finis la lecture de la partie 1 de *Du mode d'existence des objets techniques*, je reprends la fiche synthétique et je commence la rédaction du commentaire de texte.

## Jeudi 27 avril 2017
Suite de la lecture *Du mode d'existence des objets techniques*, Chapitre 1 de la Partie 1 terminée ✔

## Mercredi 26 avril 2017
Suite de la lecture *Du mode d'existence des objets techniques*.

## Mardi 25 avril 2017
Début de la lecture *Du mode d'existence des objets techniques* et de sa fiche de lecture.

Un peu de veille en rapport avec les chaînes de publication sur mon site : [www.quaternum.net/flux/](https://www.quaternum.net/flux/)

## Lundi 24 avril 2017
Écoute de la première émission d'un cycle sur Gilbert Simondon sur France Culture, voir [/fiches-de-lecture/simondon-gilbert-du-mode-d-existence-des-objets-techniques/](/fiches-de-lecture/simondon-gilbert-du-mode-d-existence-des-objets-techniques/).

## Samedi 22 avril 2017
Achat de *Du mode d'existence des objets techniques* de Gilbert Simondon.

## Jeudi 20 avril 2017 : envoi d'une réponse à Anthony Masure
Notamment concernant la fiche de lecture.

## Mercredi 19 avril 2017 : réponse d'Anthony Masure
Réponse positive d'Anthony, avec une série de recommandations et des premiers travaux à réaliser.

## Mardi 18 avril 2017 : envoi de la demande de direction à Anthony Masure
Envoi du mail avec la note introductive.

## Jeudi 6 avril 2017 : participation au workshop PrePostPrint
Suite à l'invitation de Raphaël Bastide. Rencontres : principalement Sarah Garcin et Julie Blanc.

## Lundi 13 mars 2017 : publication de l'article sur les chaînes de publication
À lire en ligne : [Une chaîne de publication inspirée du web](https://www.quaternum.net/2017/03/13/une-chaine-de-publication-inspiree-du-web/). Note : la rédaction de cet article aura mis environ quatre mois.
